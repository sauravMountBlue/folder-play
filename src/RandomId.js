import uuidv1 from 'uuid/v1';
const generateuniqueId = () => {
    return uuidv1();
}

export default generateuniqueId;