import React, { Component } from 'react';
import _ from 'lodash';
import Folder from './components/Folder';
import AddFolder from './components/AddFolder';
import uniqueId from './RandomId';
import TodoModal from './components/TodoModal';
import Todo from './components/Todo';
import './App.css';

let uid = uniqueId();

export class App extends Component {
	constructor(props) {
		super(props);
		this.state = {
			items: {
				[uid]: {
					name: 'root',
					todos: [],
					type: 'folder',
					subFolders: [],
					todosList: [],
				},
			},
			currentFolderId: uid,
			path: [uid],
			showModal: false,
			showEditModal: false,
			editableTodoId: '',
		};
	}

	// add folder function
	handleAddClick = name => {
		let currFolder = _.filter(Object.entries(this.state.items), ele => ele[0] === this.state.currentFolderId);
		let items = { ...this.state.items };
		let id = uniqueId();
		items[id] = { name, todos: [], type: 'folder', subFolders: [], todosList: [] };
		items[currFolder[0][0]].subFolders.push(id);
		this.setState({ items });
	};

	// delete folder function
	handleDeleteClick = id => {
		let currFolder = _.filter(Object.entries(this.state.items), ele => ele[0] === this.state.currentFolderId);
		let items = { ...this.state.items };
		delete items[id];
		let currFolderSubFolder = _.filter(currFolder[0][1].subFolders, ele => ele !== id);
		items[currFolder[0][0]].subFolders = [];
		items[currFolder[0][0]].subFolders.push(...currFolderSubFolder);
		this.setState({ items });
	};

	// edit folder named
	handleFolderNameChange = (id, name) => {
		let items = { ...this.state.items };
		items[id].name = name;
		this.setState({ items });
	};

	// folder double click function
	handleDoubleClick = id => {
		let path = [...this.state.path];
		path.push(id);
		this.setState({
			currentFolderId: id,
			path,
		});
	};

	// Path click
	handlePathClick = id => {
		let path = [];
		let index = this.state.path.indexOf(id);
		path = this.state.path.slice(0, index + 1);
		this.setState({ path });
		this.setState({ currentFolderId: id });
	};

	// Add todo click
	handleAddTodoClick = e => {
		this.setState({
			showModal: true,
		});
	};

	// add more task
	saveModalData = (tasks, title) => {
		let currFolder = _.filter(Object.entries(this.state.items), ele => ele[0] === this.state.currentFolderId);
		let items = { ...this.state.items };
		let id = uniqueId();
		items[id] = { name: title, todos: tasks, type: 'todo', subFolders: [], todosList: [] };
		items[currFolder[0][0]].todosList.push(id);
		this.setState({ items, showModal: false, showEditModal: false, editableTodoId: '' });
	};

	// closeModal
	closeModal = e => {
		this.setState({ showModal: false });
	};

	// edit todo
	handleEditTodoClick = id => {
		this.setState({ showEditModal: true, editableTodoId: id });
	};

	// update modal data
	updateModalData = (todos, title, id) => {
		let items = { ...this.state.items };
		items[id].name = title;
		items[id].todos = [];
		items[id].todos = todos;
		this.setState({ items, showEditModal: false });
	};

	// delete todo item
	handleTodoItemDelClick = (id, todoId) => {
		let items = { ...this.state.items };
		let todoItems = _.filter(items[todoId].todos, ele => ele.id !== id);
		items[todoId].todos = todoItems;
		this.setState({ items });
	};

	// marking checked
	handleChecked = (id, todoId, checked) => {
		let items = { ...this.state.items };
		let todoItems = _.map(items[todoId].todos, ele => {
			if (ele.id === id) {
				ele.checked = !checked;
				return ele;
			}
			return ele;
		});
		items[todoId].todos = todoItems;
		this.setState({ items });
	};

	// copy todo
	handleCopyClick = id => {
		let items = this.copyTodo(id, this.state.currentFolderId, this.state.items, false);
		this.setState({ items });
	};

	// copy todo function to
	copyTodo = (id, currentFolderId, item, fromFolder) => {
		let currFolder = _.filter(Object.entries(item), ele => ele[0] === currentFolderId);
		let items = { ...item };
		let copyItem = _.cloneDeep(item);
		let copied = _.map(copyItem[id].todos, ele => {
			let todoUid = uniqueId();
			ele.id = todoUid;
			return ele;
		});
		copyItem[id].todos = copied;
		let name = copyItem[id].name;
		if (!fromFolder) {
			if (copyItem[id].name.slice(copyItem[id].name.length - 5, copyItem[id].name.length - 1) !== 'copy') {
				copyItem[id].name = name + '(copy)';
			}
		}
		let uid = uniqueId();
		items[uid] = copyItem[id];
		if (fromFolder) {
			_.map(items[currFolder[0][0]].todosList, tdsId => {
				if (tdsId === id) {
					items[currFolder[0][0]].todosList[items[currFolder[0][0]].todosList.indexOf(tdsId)] = uid;
				}
			});
		} else {
			items[currFolder[0][0]].todosList.push(uid);
		}
		return items;
	};

	// copy folder
	handleCopyFolder = (id, currFolderId, output) => {
		let currentFolderId = currFolderId ? currFolderId : this.state.currentFolderId;
		let currFolder = _.filter(Object.entries(output), ele => ele[0] === currentFolderId);
		let folderToBeCopied = _.cloneDeep(this.state.items[id]);
		if (
			folderToBeCopied.name.slice(folderToBeCopied.name.length - 5, folderToBeCopied.name.length - 1) !==
				'copy' &&
			!currFolderId
		) {
			folderToBeCopied.name = folderToBeCopied.name + '(copy)';
		}
		let copiedFolderId = uniqueId();
		console.log(copiedFolderId);
		output[copiedFolderId] = folderToBeCopied;
		if (currFolderId) {
			_.map(output[currFolder[0][0]].subFolders, sbfId => {
				if (sbfId === id) {
					output[currFolder[0][0]].subFolders[
						output[currFolder[0][0]].subFolders.indexOf(sbfId)
					] = copiedFolderId;
				}
			});
		} else {
			output[currFolder[0][0]].subFolders.push(copiedFolderId);
		}
		_.map(output[copiedFolderId].todosList, todoId => {
			output = this.copyTodo(todoId, copiedFolderId, output, true);
		});
		_.map(output[copiedFolderId].subFolders, subFolderId => {
			this.handleCopyFolder(subFolderId, copiedFolderId, output);
		});
		if (folderToBeCopied.subFolders.length === 0) {
			this.setState({ items: output });
			return;
		}
	};

	render() {
		let currFolder = _.filter(Object.entries(this.state.items), ele => ele[0] === this.state.currentFolderId);
		let pathFolderName = [];
		_.map(this.state.path, ele => {
			_.map(Object.entries(this.state.items), subEle => {
				if (ele === subEle[0]) {
					pathFolderName.push([subEle[1].name, subEle[0]]);
				}
			});
		});
		let currTodo = _.filter(Object.entries(this.state.items), ele => ele[0] === this.state.editableTodoId);
		return (
			<div className="main">
				<div className="row">
					<span className="current-folder fa fa-folder-open text-blue">
						{_.map(pathFolderName, ele => (
							<span
								key={ele[1]}
								style={{ cursor: 'pointer' }}
								onClick={() => this.handlePathClick(ele[1])}
							>
								{ele[0]}/
							</span>
						))}
					</span>
					<AddFolder handleAddClick={this.handleAddClick} />
				</div>
				<hr />
				<div style={{ display: 'flex', flexWrap: 'wrap', marginTop: '20px' }}>
					{_.map(Object.entries(this.state.items), ele =>
						ele[1].type === 'folder' ? (
							currFolder[0][1].subFolders.includes(ele[0]) ? (
								<Folder
									id={ele[0]}
									key={ele[0]}
									name={ele[1].name}
									items={{ ...this.state.items }}
									handleDeleteClick={this.handleDeleteClick}
									handleDoubleClick={this.handleDoubleClick}
									handleFolderNameChange={this.handleFolderNameChange}
									handleCopyFolder={this.handleCopyFolder}
								/>
							) : null
						) : currFolder[0][1].todosList.includes(ele[0]) ? (
							<Todo
								todos={ele[1].todos}
								title={ele[1].name}
								key={ele[0]}
								id={ele[0]}
								handleEditTodoClick={this.handleEditTodoClick}
								handleTodoItemDelClick={this.handleTodoItemDelClick}
								handleChecked={this.handleChecked}
								handleCopyClick={this.handleCopyClick}
							/>
						) : null
					)}
					<span
						className="add-Todo"
						style={{
							backgroundColor: '#a1a2a3',
							marginTop: '5px',
							height: 'fit-content',
							padding: '5px',
							cursor: 'pointer',
						}}
						onClick={this.handleAddTodoClick}
					>
						Add new todo
					</span>
				</div>
				{this.state.showModal ? (
					<TodoModal
						tasks={[{ id: uniqueId(), desc: '', checked: false }]}
						title={''}
						saveModalData={this.saveModalData}
						closeModal={this.closeModal}
						editModalState={false}
					/>
				) : null}
				{this.state.showEditModal ? (
					<TodoModal
						tasks={currTodo[0][1].todos}
						title={currTodo[0][1].name}
						id={currTodo[0][0]}
						updateModalData={this.updateModalData}
						closeModal={this.closeModal}
						editModalState={true}
					/>
				) : null}
			</div>
		);
	}
}

export default App;
