import React, { Component } from 'react';

export class Todo extends Component {
	render() {
		return (
			<div className="todoMain">
				<div className="row">
					<span>{this.props.title}</span>
					<span className="editTodo" onClick={() => this.props.handleEditTodoClick(this.props.id)}>
						Edit
					</span>
				</div>
				<div className="todo-items">
					{this.props.todos.map(ele => (
						<div key={ele.id}>
							<input
								type="checkbox"
								checked={ele.checked}
								onChange={e => this.props.handleChecked(ele.id, this.props.id, ele.checked)}
							/>
							<span
								className="item-desc"
								style={{ textDecoration: ele.checked ? 'line-through' : 'none' }}
							>
								{ele.desc}
							</span>

							<span
								className="fa fa-trash"
								onClick={() => {
									this.props.handleTodoItemDelClick(ele.id, this.props.id);
								}}
							></span>
						</div>
					))}
				</div>
                <div className="copy-todo">
                    <button type="button" className="create-folder-link" onClick={() => this.props.handleCopyClick(this.props.id)}>copy</button>
                </div>
			</div>
		);
	}
}

export default Todo;
