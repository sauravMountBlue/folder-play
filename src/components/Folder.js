import React, { Component } from 'react';

export class Folder extends Component {
	constructor(props) {
		super(props);
		this.state = {
			toggleInput: false,
			folderName: this.props.name
		};
	}
	handleClick = e => {
		this.setState({ toggleInput: true });
	};
	handleChange = e => {
		this.setState({ folderName: e.target.value });
	}
	render() {
		return (
			<div style={{ marginRight: '40px' }}>
				<span
					className="fa fa-folder text-blue"
					onDoubleClick={() => this.props.handleDoubleClick(this.props.id)}
				></span>
				<br />
				{this.state.toggleInput ? (
					<div>
						<input className="editInputTodo" value={this.state.folderName} onChange={this.handleChange} />
						<button className="btn-success" type="button" onClick={() => { this.setState({toggleInput: false}); return this.props.handleFolderNameChange(this.props.id, this.state.folderName)}}>save</button>
					</div>
				) : (
					<span className="folder-name" onClick={this.handleClick}>
						{this.props.name}
					</span>
				)}
				<br />
				<button className="create-folder-link" style={{marginTop: '5px', width: '80px'}} type="button" onClick={() => this.props.handleCopyFolder(this.props.id, null, this.props.items)}>copy</button> <br />
				<span className="fa fa-trash text-red" onClick={() => this.props.handleDeleteClick(this.props.id)}>
					<span style={{ fontSize: '14px' }}>Delete</span>
				</span>
			</div>
		);
	}
}

export default Folder;
