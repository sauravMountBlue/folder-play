import React, { Component } from 'react'

export class AddFolder extends Component {
    constructor (props) {
        super(props);
        this.state = {
            folderName: ''
        }
    }
    handleChange = e => {
        this.setState({
            folderName: e.target.value
        })
    }
    render() {
        return (
            <div className="folder-name-input">
                <input className="folder-name" placeholder="Enter folder name" value={this.state.folderName} onChange={this.handleChange} />
                <button type="button" className="create-folder-link" onClick={() => {this.setState({folderName: ''}); return this.props.handleAddClick(this.state.folderName)}}>Add Folder</button>
            </div>
        )
    }
}

export default AddFolder
