import React, { Component } from 'react';
import Modal from 'react-modal';
import _ from 'lodash';
import uniqueId from '../../RandomId';

Modal.setAppElement('#root');

const customStyle = {
	content: {
		top: '50%',
		left: '50%',
		right: 'auto',
		bottom: 'auto',
		marginRight: '-50%',
		transform: 'translate(-50%, -50%)',
	},
};

export class TodoModal extends Component {
	constructor(props) {
		super(props);
		this.state = {
			tasks: this.props.tasks,
			title: this.props.title,
			editModal: this.props.editModalState,
		};
	}

	// handle input change
	handleChange = e => {
		let tasks = [...this.state.tasks];
		let tasksNew = _.map(tasks, ele => {
			if (ele.id === e.target.id) {
				ele.desc = e.target.value;
				return ele;
			} else {
				return ele;
			}
		});
		this.setState({ tasks: tasksNew });
	};

	// appending more task desc fields
	addMoreTaskField = e => {
		let tasks = [...this.state.tasks];
		tasks.push({ id: uniqueId(), desc: '', checked: false });
		this.setState({ tasks });
	};

	// title change event
	handleTitleChange = e => {
		this.setState({ title: e.target.value });
	};

	// saving todo
	handleSaveTodo = e => {
		let tasks = [...this.state.tasks];
		let title = this.state.title;
		this.setState({
			tasks: [
				{
					id: uniqueId(),
					desc: '',
					checked: false
				},
			],
			title: '',
		});
		return this.props.saveModalData(tasks, title);
	};

	// update todo
	handleUpdateTodo = e => {
		let tasks = [...this.state.tasks];
		let title = this.state.title;
		return this.props.updateModalData(tasks, title, this.props.id);
	};

	render() {
		return (
			<Modal isOpen={true} onRequestClose={this.closeModal} style={customStyle}>
				<div className="modal-header p-1">
					<h4>Add a new todo</h4>
				</div>
				<hr />
				<div className="modal-body p-1">
					<form>
						<input
							placeholder="Enter title of todo"
							value={this.state.title}
							onChange={this.handleTitleChange}
							className="todoInput"
						/>
					</form>
					<h5 className="mx-1">Add items</h5>
					{_.map(this.state.tasks, ele => (
						<input
							placeholder="Enter task description"
							key={ele.id}
							id={ele.id}
							value={ele.desc}
							onChange={this.handleChange}
							className="todoInput"
						/>
					))}
					<p className="mx-1 addMoreItem" onClick={this.addMoreTaskField}>
						Add more items
					</p>
				</div>
				<hr />
				<div className="modal-footer p-1">
					{this.state.editModal ? (
						<button onClick={this.handleUpdateTodo} className="create-folder-link">
							update 
						</button>
					) : (
						<button onClick={this.handleSaveTodo} className="create-folder-link">
							save <span className="fa fa-check"></span>
						</button>
					)}
					<button onClick={this.props.closeModal} className="cross-btn">
						close &times;
					</button>
				</div>
			</Modal>
		);
	}
}

export default TodoModal;
